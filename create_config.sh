#!/bin/bash

SCRIPT_NAME="$(basename "$0")"

# if using symlinks, this is the symlink file
# SCRIPT_DIR="$(cd "$(dirname "$0")" && pwd -P)"
# resolve simlinks
SCRIPT_SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SCRIPT_SOURCE" ]; do # resolve $SCRIPT_SOURCE until the file is no longer a symlink
  SCRIPT_DIR="$( cd -P "$( dirname "$SCRIPT_SOURCE" )" && pwd )"
  SCRIPT_SOURCE="$(readlink "$SCRIPT_SOURCE")"
  [[ $SCRIPT_SOURCE != /* ]] && SCRIPT_SOURCE="$SCRIPT_DIR/$SCRIPT_SOURCE" # if $SCRIPT_SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
SCRIPT_DIR="$( cd -P "$( dirname "$SCRIPT_SOURCE" )" && pwd )"


function usage {
    echo "${SCRIPT_NAME} [--help] profile_name [client1 client2 ... clientN]"
    echo "Creates a new screen profile PROFILE_NAME."
    echo "Optionally adds the clients CLIENT1 to CLIENTN to that profile."
    echo ""
    echo "(Using profile names starting with <sim> allows"
    echo "to access the clients with the <sim>-prefix)"
    echo ""
    echo "Example:"
    echo "${SCRIPT_NAME} simRocket stu28 prakt12"
}

if [ "$#" -eq 0 ] || [ "$1" == "--help" ]; then
    usage
    exit
fi

CONFIGNAME=$1

if [ -f "${CONFIGNAME}.screenrc" ]; then
    echo "Profile ${CONFIGNAME} exists already."
else
    {
	echo "sessionname \$SCREEN_SESSION_NAME"
	echo "source common.screenrc"
	echo "chdir /"
	echo ""
	echo "# open ssh connections"
	echo "source ${CONFIGNAME}.list.screenrc"
	echo ""
	echo "# make layout persistent"
	echo "layout save default"
    } >> "${CONFIGNAME}.screenrc"
fi

shift

if [ "$#" -gt 0 ]; then
    if [ -f "${CONFIGNAME}.list.screenrc" ]; then
	echo "Updating list of simulation clients."
	mv "${CONFIGNAME}.list.screenrc" "${CONFIGNAME}.list.screenrc.bak"
    fi
    {
	for ARG in "$@"; do
	    echo "screen -t \${SCREENPREFIX}${ARG} ssh ${ARG}"
	done
    } > "${CONFIGNAME}.list.screenrc"

fi

