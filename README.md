# Screen Configuration Wrapper

## Usage
- Create a screen configuration: `create_config.sh foo bar1 bar2 bar2`
  where foo is the config name and bar1, bar2, bar3 are the hosts
  you want to connect to via ssh at screen startup
  - files `foo.screenrc` and `foo.list.screenrc` are generated
  -
- Customize `common.screenrc` to fit your needs
- Open screen using the created configuration: `scr.sh foo`

For convenience, you can symlink `scr.sh` and `create_config.sh` to your bin folder.

## Files
- `scr.sh`: main wrapper script
- `create_config.sh`: configuration creation script
- `common.screenrc`: common screen configuration file
