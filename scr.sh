#!/bin/bash

# https://unix.stackexchange.com/questions/318865/gnu-screen-load-multiple-configuration-files

SCRIPT_NAME="$(basename "$0")"
# resolve simlinks
SCRIPT_SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SCRIPT_SOURCE" ]; do # resolve $SCRIPT_SOURCE until the file is no longer a symlink
  SCRIPT_DIR="$( cd -P "$( dirname "$SCRIPT_SOURCE" )" && pwd )"
  SCRIPT_SOURCE="$(readlink "$SCRIPT_SOURCE")"
  [[ $SCRIPT_SOURCE != /* ]] && SCRIPT_SOURCE="$SCRIPT_DIR/$SCRIPT_SOURCE" # if $SCRIPT_SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
SCRIPT_DIR="$( cd -P "$( dirname "$SCRIPT_SOURCE" )" && pwd )"

function usage {
    echo "Usage: ${SCRIPT_NAME} [--name NAME, --prefix NAME, --help] CONFIGURATION"
    echo "Open screen using CONFIGURATION as config file."
}

function help {
    usage
    echo ""
    echo "--help          Show this help text."
    echo "--name NAME     Use NAME as session name." \
	 " By default, the configuration name is used."
    echo "--prefix NAME   Use NAME as screen window prefix." \
	 "By default, if configuration name starts with the \"sim\" keyword, the prefix is \"sim-\""
    echo ""
    echo "Valid configurations are:"

CONFIGS=$(find "$SCRIPT_DIR" -type f -name "*.screenrc" -not -name "*.list.*" -not -name "common*" -exec sh -c 'tmp=$0; tmp=${tmp%.*}; tmp=${tmp##*/}; echo $tmp' '{}' \;)
    
    # CONFIGS=$(find . -type f -name "*.screenrc" -not -name "*.list.*" -not -name "common*")
    echo "${CONFIGS[*]}"
    
}


if [ "$#" -lt 1 ]; then
    usage
    exit
fi


### simple ui (no POSIX support)
## default values
HELP=false
NAME=false
NAMEVAL=""
PREFIX=false
SCREENPREFIX=""
## parser loop
while [[ "$1" == -* ]]; do
    case "$1" in
        -h | --help)
	    HELP=true;;
	# mandatory
	-n | --name)
	    NAME=true;
	    NAMEVAL="$2"; shift;;
	-p | --prefix)
	    PREFIX=true;
	    SCREENPREFIX="$2"; shift;;
        --)
	    shift; break;; # end of options
    esac
    shift
done


if [ "$HELP" == true ]; then
    help
    exit
fi


CONFIG_NAME=$1
shift


if [ "$NAME" == true ]; then
    SCREEN_SESSION_NAME=$NAMEVAL
else
    SCREEN_SESSION_NAME=$CONFIG_NAME
fi
export SCREEN_SESSION_NAME

RC_FILE="${SCRIPT_DIR}/${CONFIG_NAME}.screenrc"

if [ "$PREFIX" == false ]; then
    if [[ "$CONFIG_NAME" == sim* ]]; then
	SCREENPREFIX="sim-"
    fi
fi
export SCREENPREFIX

# SCREENPOSTFIX=""
# if [[ "$SCREEN_SESSION_NAME" == *two ]]; then
#     SCREENPOSTFIX=""
# fi
# export SCREENPOSTFIX

# attach to running (detach other remote connection first) or create new screen
exec screen -c "$RC_FILE" -S "$SCREEN_SESSION_NAME" -r -R -d "$@"
